<?php
/**
 * Created by PhpStorm.
 * User: gilzowp
 * Date: 5/4/16
 * Time: 8:42 AM
 */
/*
?><h1><?php echo get_the_title();?></h1>
<?php the_content();?>
<p>Now we'll start working through all the meta data:</p>


<?php if(have_rows('flexcontentkey')){
    echo '<p>ok, our flex content area has rows.</p>',PHP_EOL;
    while (have_rows('flexcontentkey')){
        the_row();
        $row = get_row();
        echo '<p>here are the contents of the_row while looping through flexcontentkey:</p><pre>',PHP_EOL;
        echo var_export($row);
        echo '</pre>',PHP_EOL;
    }

}*/
namespace cheese\controllers;
use MizzouMVC\controllers\Main;
use cheese\models\Posts;

class Page extends Main
{
    public function main()
    {
        $objPosts = $this->load('cheese\models\Posts');
        $this->renderData('MainPost',$objPosts->convertPost($this->post,array('include_meta'=>true)));
        $this->render('page');
    }
}
new Page();
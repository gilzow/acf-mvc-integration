<?php
/**
 * 
 * TL;DR description 
 *
 * @package 
 * @subpackage 
 * @category 
 * @category 
 * @author Paul F. Gilzow, Web Communications, University of Missouri
 * @copyright 2016 Curators of the University of Missouri
 */

namespace cheese\models;
use MizzouMVC\models\WpBase;
use cheese\models\MizzouPostACF;
require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'MizzouPostACF.php';

class Posts extends WpBase {

    protected function _newPostInstance($objPost,$aryOptions)
    {
        return new MizzouPostACF($objPost,$aryOptions);
    }
}
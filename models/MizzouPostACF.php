<?php
/**
 * 
 * TL;DR description 
 *
 * @package 
 * @subpackage 
 * @category 
 * @category 
 * @author Paul F. Gilzow, Web Communications, University of Missouri
 * @copyright 2016 Curators of the University of Missouri
 *
 * wy_ = wysiwyg
 * flex = flexlayout group
 * rpt = repeater groups
 *
 * should we just do [^_]+ for flexlayout group?
 */

namespace cheese\models;
use MizzouMVC\models\MizzouPost;

class MizzouPostACF extends MizzouPost {

    public function __construct($mxdPost,$aryOptions=array())
    {
        parent::__construct($mxdPost,$aryOptions);
        /**
         * We cn have multiple flexible content groups.
         * Each flexible content group can have multiple content layers
         * each layer can have multiple fields
         */
        $aryFlexGroups = array();
        if(!$this->isError()){
            $strPattern = '/^(layout[^_]+)$/';

            $aryFlexLayouts = preg_grep($strPattern,array_keys($this->aryOriginalCustomData));
            foreach($aryFlexLayouts as $strFlexGroupKey){
                //this should be a flexcontent group
                if(have_rows($strFlexGroupKey)){
                    $aryLayouts = array();
                    while(have_rows($strFlexGroupKey)){
                        the_row();//acf specific function to set up its loop
                        /**
                         * @todo what other info do we need to store concerning the layout?  if nothing, do we need it
                         * to be an oject with a fields array?  Or just an array of fields?
                         */
                        $objLayout = new \stdClass();
                        $objLayout->fields = array();
                        $aryLayoutLayer = get_row();
                        $strLayoutLayerKey = get_row_layout();
                        $objLayout->name = $strLayoutLayerKey;

                        foreach ($aryLayoutLayer as $strLayoutFieldKey => $strLayoutFieldValue) {
                            /*
                            * This is extremely brittle. if the author decides to change this key name, you'll have to
                            * update it here.  However, after looking through his code, he has it sprinkled everywhere,
                             * so it is unlikely he will change it without major modification to his codebase
                            */

                            if(0 === strpos($strLayoutFieldKey,'field_')){
                                //now we need to see if we can find the custom meta field key that corresponds to this key
                                $aryField = get_sub_field_object($strLayoutFieldKey);

                                if(false !== $aryField){
                                    //$this->d('the key is a field. we need an object',$strFieldKey);
                                    $objField = new \stdClass();
                                    $objField->key = $aryField['name'];
                                    $objField->internal_id = $aryField['key'];
                                    $objField->type = $aryField['type'];
                                    $objField->raw_contents = $strLayoutFieldValue;

                                    switch($objField->type){
                                        case 'image':
                                            $mxdContents = $this->_convertImage($aryField['value']);
                                            break;
                                        case 'gallery':
                                            $mxdContents = array();
                                            foreach ($aryField['value'] as $aryImage){
                                                $mxdContents[] = $this->_convertImage($aryImage);
                                            }
                                            break;
                                        case 'relationship':
                                            //passthru done intentionally
                                        case 'post_object':
                                            $mxdContents = $this->_convertPost($aryField['value']);
                                            break;
                                        case 'user':
                                            //passthru done intentionally
                                        case 'taxonomy':
                                            //passthru done intentionally
                                        default:
                                            $mxdContents = $aryField['value'];
                                            break;
                                    }

                                    $objField->contents = $mxdContents;
                                    //now add the field to our layout
                                    $objLayout->fields[$objField->key] = $objField;
                                }
                            }
                        }
                        $aryLayouts[$objLayout->name] = $objLayout;
                    }

                    $aryFlexGroups[$strFlexGroupKey] = $aryLayouts;
                }
            }

            $this->add_data('flexgroups',$aryFlexGroups);
        }
    }

    protected function _convertPost($mxdPost)
    {
        return $this->_newPostInstance($mxdPost);
    }

    protected function _convertImage($mxdImage)
    {
        //we call the link to the full size image 'src_full', where he calls it 'url' so lets add ours
        $mxdImage['src_full'] = $mxdImage['url'];

        /**
         * next thing we need to do is extract all of the image size names. we know that all of them have a name-height
         * key, so we'll use that to get all of the image size names
         */
        $strImagePattern = '/^([^-]+)\-height$/';
        $aryImageSizeKeys = preg_grep($strImagePattern,array_keys($mxdImage['sizes']));
        //we now have a list of matching KEYS. let's reduce that to just the sizes
        if(count($aryImageSizeKeys) > 0 ){
            $aryImageSizes = array();
            foreach ($aryImageSizeKeys as $strImageSizeKey){
                if(1 === preg_match($strImagePattern,$strImageSizeKey,$aryImageMatch)){
                    $mxdImage['src_' . $aryImageMatch[1]] = $mxdImage['sizes'][$aryImageMatch[1]];
                }
            }
        }

        return $this->_newImageInstance($mxdImage);
    }

    protected function _instantiateNewPost($mxdPost,$aryOptions = array())
    {
        $objNewPost = $this->_newPostInstance($mxdPost,$aryOptions);
        if(is_subclass_of($objNewPost,'\MizzouMVC\models\MizzouPost') || is_a($objNewPost,'\MizzouMVC\models\MizzouPost')){
            return $objNewPost;
        } else {
            $strMsg = 'object returned from self::_newPostInstance must be an instance of MizzouPost or a child instance of MizzouPost. Therefore I\'ve left it untouched.';
            $this->add_error($strMsg);
            return $mxdPost;
        }
    }

    /**
     * Here so child classes can override to inject custom class
     * @param integer|\WP_Post $mxdPost
     * @param array $aryOptions
     * @return object mixed
     */
    protected function _newPostInstance($mxdPost,$aryOptions = array())
    {
        return new parent($mxdPost,$aryOptions);
    }

    protected function _newImageInstance($mxdImage)
    {
        return (object)$mxdImage;
    }

    /**
     * Temporary debug function
     * @param string $strMsg
     * @param mixed $mxdExport
     * @return null
     * @deprecated
     */
    public function d($strMsg,$mxdExport=null)
    {
        return null;
    }
}
<?php
/**
 * 
 *
 * @package Wordpress
 * @subpackage SITENAME
 * @category theme
 * @category controller
 * @uses MizzouMVC\controllers\Main
 * @author Paul F. Gilzow, Web Communications, University of Missouri
 * @copyright 2016 Curators of the University of Missouri
 */

//change this to the namespace you'll use for your theme
namespace cheese\controllers;
use MizzouMVC\controllers\Main;


class Index extends Main {

	public function main()
	{
		$this->render('index');
	}
}

new Index();
<?php
/**
 * Functions, actions, filters, etc used in the theme
 *
 * @package WordPress
 * @subpackage SITENAME
 * @category theme
 * @category functions
 * @author Paul F. Gilzow & Jason L Rollins, Web Communications, University of Missouri
 * @copyright 2016 Curators of the University of Missouri
 * @version 1.0.1
 */

define('MIZZOUMVC_COMPATIBLE',true);

add_image_size('specialimage','600','400',true);